package menko;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SmsExample {

    public static final String ACCOUNT_SID = "AC7c584fb82084bb66dad13836cd64785d";
    public static final String AUTH_TOKEN = "75481ab83cb4e3eb9625d5905c836a3a";

    public static void send(String msg) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380636190598"),
                        new PhoneNumber("+15745384771"), msg).create();
    }
}
